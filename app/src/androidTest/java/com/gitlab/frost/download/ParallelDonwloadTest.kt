package com.gitlab.frost.download

import androidx.room.Room
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.gitlab.frost.frostdown.dispatcher.ParallelDownload
import com.gitlab.frost.frostdown.fileinfo.DBFileInfo
import com.gitlab.frost.frostdown.fileinfo.DownloadDatabase
import com.gitlab.frost.frostdown.fileinfo.DownloadEntity
import com.gitlab.frost.frostdown.fileinfo.DownloadTaskEntity
import com.gitlab.frost.frostdown.network.OkHttpAdapter
import com.gitlab.frost.frostdown.network.Request
import io.reactivex.internal.operators.single.SingleDoOnSuccess
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ParallelDonwloadTest {
    @Test
    fun test() {
        val response = OkHttpAdapter().getResponse(
            Request(
                "http://local.lan/files/DerpFest-10-libra-%E5%B0%8F%E7%B1%B34C.zip",
                listOf(
                    "Range" to "0-"
                )
            )
        )
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        val db = Room.databaseBuilder(
            appContext,
            DownloadDatabase::class.java,
            "test"
        ).build()
        val entity = DownloadEntity(
            "http://local.lan/files/DerpFest-10-libra-%E5%B0%8F%E7%B1%B34C.zip",
            appContext.externalCacheDir!!.path,
            response.getETag(),
            response.getLastModified(),
            response.getContentLength()
        )
        db.downloadDao()
            .insertDownloads(
                entity
            )
            .subscribe()

        db.downloadDao().queryDownload(
            "http://local.lan/files/DerpFest-10-libra-%E5%B0%8F%E7%B1%B34C.zip",
            appContext.externalCacheDir!!.path.toString()
        )
            .subscribe {
                db.downloadTaskDao().insertDownloadTasks(
                    DownloadTaskEntity(
                        it.first().id,
                        0,
                        response.getContentLength(),
                        0
                    )
                ).subscribe()
                ParallelDownload(DBFileInfo(it.first(), db), OkHttpAdapter()).start()
            }
        Thread.sleep(100000)
    }
}