//package com.gitlab.frost.downloadutil.databasetest
//
//import android.util.Log
//import androidx.room.Room
//import androidx.test.ext.junit.runners.AndroidJUnit4
//import androidx.test.platform.app.InstrumentationRegistry
//import com.gitlab.frost.frostdown.fileinfo.*
//import org.junit.After
//import org.junit.Before
//import org.junit.Test
//import org.junit.runner.RunWith
//import java.util.*
//
//@RunWith(AndroidJUnit4::class)
//class DownloadTest {
//    private val TAG = this.javaClass.name
//    lateinit var db: DownloadDatabase
//    lateinit var downloadDao: DownloadDao
//    lateinit var downloadTaskDao: DownloadTaskDao
//
//    @Before
//    fun createDB() {
//        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
//
//        db = Room.databaseBuilder(
//            appContext,
//            DownloadDatabase::class.java, "download_test"
//        ).build()
//
//        downloadDao = db.downloadDao()
//        downloadTaskDao = db.downloadTaskDao()
//    }
//
//    @Test
//    fun test() {
//        query()
//        insert()
//        query()
//        update()
//        query()
//        delete()
//        query()
//    }
//
//    private fun insert() {
//        log("insert")
//        downloadDao.insertDownloads(
//            DownloadEntity(
//                "test",
//                "test111111",
//                "test",
//                "test",
//                0L,
//                0L,
//                12,
//                Date().time
//            )
//        )
//        val id = downloadDao.queryDownloads().first().id
//        downloadTaskDao.insertDownloadTasks(DownloadTaskEntity(id, 0, 33333, 0))
//        downloadTaskDao.insertDownloadTasks(DownloadTaskEntity(id, 33330, 33333, 0))
//        downloadTaskDao.insertDownloadTasks(DownloadTaskEntity(id, 232130, 33333, 0))
//        downloadTaskDao.insertDownloadTasks(DownloadTaskEntity(id, 333311, 33333, 0))
//    }
//
//    private fun log(name: String) {
//        Log.d(TAG, "---------$name--------")
//    }
//
//    fun update() {
//        log("update")
//        downloadDao.queryDownloads().forEach {
//            it.fileName = "21312312"
//            downloadDao.updateDownload(it)
//        }
//    }
//
//    fun query() {
//        log("query")
//        downloadDao.queryDownloads().forEach {
//            Log.d(TAG, it.toString())
//            downloadTaskDao.queryDownloadTask(it.id).forEach { task ->
//                Log.d(TAG, task.toString())
//            }
//        }
//
//    }
//
//    fun delete() {
//        log("delete")
//        downloadDao.deleteDownload(
//            downloadDao.queryDownloads().first().also {
//                downloadTaskDao.queryDownloadTask(it.id)
//                    .forEach {
//                        downloadTaskDao.deleteDownloadTasks(it)
//                    }
//            }
//        )
//    }
//
//    @After
//    fun close() {
//        db.close()
//    }
//
//}