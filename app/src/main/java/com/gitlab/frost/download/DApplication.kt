package com.gitlab.frost.download

import android.app.Application
import com.gitlab.frost.logger.LogBuilder
import com.gitlab.frost.logger.Logger

class DApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        Logger.init(
            LogBuilder()
                .setFilename("download_test_log")
                .setMode(LogBuilder.LOG_MODE.LOGCAT)
                .setLogPath(externalCacheDir?.path ?: cacheDir.path + "log")
                .build()
        )
    }
}