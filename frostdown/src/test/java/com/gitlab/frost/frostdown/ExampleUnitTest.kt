package com.gitlab.frost.frostdown

import io.reactivex.*
import io.reactivex.schedulers.Schedulers
import okhttp3.internal.notify
import okhttp3.internal.notifyAll
import okhttp3.internal.threadFactory
import okhttp3.internal.wait
import org.junit.Test

import org.junit.Assert.*
import java.lang.Exception

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    val signle = Schedulers.single()
    var isStop = false
    val a = Any()
    @Test
    fun addition_isCorrect() {

        repeat(3) {
            Observable.create<Long> { e ->
                println(Thread.currentThread().name)
                try {
                    repeat(10) {
                        stop()
                        Thread.sleep(1000)

                        e.onNext(it.toLong())
                    }
                } catch (ex: Exception) {
                    e.onError(ex)
                } finally {
                    e.onComplete()
                }
            }
                .subscribeOn(Schedulers.io())
                .observeOn(signle)
                .subscribe({
                    println(Thread.currentThread())
                    println(it)
                },
                    {
                        //    println(it.message)
                    },
                    {
                        println("completed")
                    })
        }
        Thread.sleep(4000)
        isStop = true
        Thread.sleep(3000)
        start()
        Thread.sleep(3333)
        isStop = true
        Thread.sleep(30200)
    }

    private fun start() {
        synchronized(this) {
            isStop = false
            this.notifyAll()
        }
    }

    private fun stop() {
        if (isStop)
            synchronized(this
            ) {
                this.wait()
            }
    }
}