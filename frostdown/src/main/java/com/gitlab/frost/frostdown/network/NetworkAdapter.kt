package com.gitlab.frost.frostdown.network

import java.io.Closeable
import java.io.InputStream

internal interface NetworkAdapter<T : NetworkAdapter.ResponseAdapter> {
    interface ResponseAdapter :Closeable{
        fun acceptRange(): Boolean
        fun getCode(): Int
        fun onResponse(
            handler: (InputStream) -> Unit,
            onCatch: (Exception) -> Unit,
            onFinally: () -> Unit
        )

        fun getETag(): String
        fun getLastModified(): Long
        fun getContentLength(): Long
    }

    fun getResponse(request: Request): T
}

data class Request(
    val url: String,
    val headers: List<Pair<String, String>>
)

abstract class AbstractNetworkAdapter<T : AbstractResponseAdapter> :
    NetworkAdapter<T> {
}

abstract class AbstractResponseAdapter : NetworkAdapter.ResponseAdapter
