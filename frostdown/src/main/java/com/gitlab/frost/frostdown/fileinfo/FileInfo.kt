package com.gitlab.frost.frostdown.fileinfo

import java.io.File
import java.lang.reflect.InvocationHandler


typealias Range<A, B> = Pair<A, B>

interface FileInfo {

    fun saveDir(): String

    fun fileName(): String

    fun getBlocks(file:File,handler: (List<BlockInfo>)->Unit)

    fun getUrl(): String

    fun getTotal():Long
}

interface BlockInfo {
    fun downloadAgain(value: Boolean = false)
    fun needRange(): Range<Long, Long>
    fun updateCurrent(value: Long)
    fun getTag(): Long
    fun getDownloadLength():Long
    fun writeFile(bytes: ByteArray, len: Int)
}
