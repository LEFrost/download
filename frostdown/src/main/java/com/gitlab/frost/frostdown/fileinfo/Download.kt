package com.gitlab.frost.frostdown.fileinfo

import android.webkit.URLUtil
import androidx.room.*
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.*

object DownloadContract {
    const val TABLE_NAME = "download"
    const val URL = "url"
    const val FILE_NAME = "file_name"
    const val SAVE_DIR = "save_dir"
    const val E_TAG = "e_tag"
    const val LAST_MODIFIED = "last_modified"
    const val TOTAL = "total"
    const val ID = "_id"
    const val START_TIME = "start_time"
    const val END_TIME = "end_time"
}

@Entity(tableName = DownloadContract.TABLE_NAME)
data class DownloadEntity(
    @ColumnInfo(name = DownloadContract.URL) val url: String,
    @ColumnInfo(name = DownloadContract.SAVE_DIR) val saveDir: String,
    @ColumnInfo(name = DownloadContract.E_TAG) var eTag: String?,
    @ColumnInfo(name = DownloadContract.LAST_MODIFIED) var lastModified: Long?,
    @ColumnInfo(name = DownloadContract.TOTAL) var total: Long,
    @ColumnInfo(name = DownloadContract.FILE_NAME) var fileName: String = URLUtil.guessFileName(
        url,
        null,
        null
    ),
    @ColumnInfo(name = DownloadContract.START_TIME) val startTime: Long = Date().time,
    @ColumnInfo(name = DownloadContract.END_TIME) var endTime: Long = 0,

    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = DownloadContract.ID) var id: Long = 0
)

@Dao
interface DownloadDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDownloads(vararg downloads: DownloadEntity): Single<LongArray>

    @Query("select * from ${DownloadContract.TABLE_NAME}")
    fun queryDownloads(): Flowable<Array<DownloadEntity>>

    @Query(
        "select * from ${DownloadContract.TABLE_NAME}" +
                " where ${DownloadContract.URL} =:url and ${DownloadContract.SAVE_DIR}=:saveDir"
    )
    fun queryDownload(url: String, saveDir: String): Flowable<Array<DownloadEntity>>

    @Query(
        "select * from ${DownloadContract.TABLE_NAME} where ${DownloadContract.ID}=:id"
    )
    fun queryDownloadById(id: Int): Flowable<DownloadEntity>

    @Delete
    fun deleteDownload(download: DownloadEntity): Single<Int>

    @Update
    fun updateDownload(vararg downloads: DownloadEntity): Single<Int>

}