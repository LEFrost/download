package com.gitlab.frost.frostdown.fileinfo

import android.annotation.SuppressLint
import java.io.File
import java.io.RandomAccessFile

class DBFileInfo(
    private val entity: DownloadEntity,
    private val database: DownloadDatabase
) : FileInfo {

    override fun saveDir(): String {
        return entity.saveDir
    }

    override fun fileName(): String {
        return entity.fileName
    }

    @SuppressLint("CheckResult")
    override fun getBlocks(file: File, handler: (List<BlockInfo>) -> Unit) {
        database.downloadTaskDao().queryDownloadTask(entity.id).subscribe {
            handler(it.map { task ->
                DBBlockInfo(task, database,file)
            })
        }
    }

    override fun getUrl(): String {
        return entity.url
    }

    override fun getTotal(): Long {
        return entity.total
    }

}

class DBBlockInfo(
    private val downloadTaskEntity: DownloadTaskEntity,
    private val database: DownloadDatabase,
    val file: File
) : BlockInfo {
    private val randomAccessFile: RandomAccessFile = RandomAccessFile(file, "rw")

    init {
    }

    private var isNew = false
    override fun downloadAgain(value: Boolean) {
        isNew = value
        if (value) {
            downloadTaskEntity.current = 0
            database.downloadTaskDao().updateDownloadTasks(downloadTaskEntity)
        }
        randomAccessFile.seek(downloadTaskEntity.current)
    }

    override fun needRange(): Range<Long, Long> {
        return downloadTaskEntity.current to downloadTaskEntity.end
    }


    override fun updateCurrent(value: Long) {
        downloadTaskEntity.current = value
        database.downloadTaskDao().updateDownloadTasks(downloadTaskEntity)
    }

    override fun getTag(): Long {
        return downloadTaskEntity.id
    }

    override fun getDownloadLength(): Long {
        return downloadTaskEntity.current - downloadTaskEntity.start - 1
    }

    override fun writeFile(bytes: ByteArray, len: Int) {
        randomAccessFile.write(bytes, 0, len)
        downloadTaskEntity.current += len
        database.downloadTaskDao().updateDownloadTasks(downloadTaskEntity).subscribe()
    }

}