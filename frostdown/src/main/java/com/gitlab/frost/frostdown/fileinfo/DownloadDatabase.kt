package com.gitlab.frost.frostdown.fileinfo

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    entities = arrayOf(DownloadEntity::class, DownloadTaskEntity::class),
    version = 3
)
abstract class DownloadDatabase : RoomDatabase() {
    abstract fun downloadDao():DownloadDao
    abstract fun downloadTaskDao(): DownloadTaskDao
}