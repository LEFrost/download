package com.gitlab.frost.frostdown.fileinfo

import androidx.room.*
import io.reactivex.Flowable
import io.reactivex.Single


object DownloadTaskContract {
    const val TABLE_NAME = "download_task"
    const val DOWNLOAD_ID = "download_id"
    const val START_INDEX = "start"
    const val END_INDEX = "end"
    const val CURRENT_INDEX = "current"
    const val ID = "_id"
}

@Entity(
    tableName = DownloadTaskContract.TABLE_NAME, foreignKeys = arrayOf(
        ForeignKey(
            entity = DownloadEntity::class,
            parentColumns = arrayOf(DownloadContract.ID),
            childColumns = arrayOf(DownloadTaskContract.DOWNLOAD_ID)
        )
    )
)
data class DownloadTaskEntity(
    @ColumnInfo(name = DownloadTaskContract.DOWNLOAD_ID) val downloadId: Long,
    @ColumnInfo(name = DownloadTaskContract.START_INDEX) var start: Long,
    @ColumnInfo(name = DownloadTaskContract.END_INDEX) var end: Long,
    @ColumnInfo(name = DownloadTaskContract.CURRENT_INDEX) var current: Long,
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = DownloadTaskContract.ID) var id: Long = 0
)

@Dao
interface DownloadTaskDao {
    @Query(
        "select * from ${DownloadTaskContract.TABLE_NAME} " +
                "where ${DownloadTaskContract.DOWNLOAD_ID}=:downloadId"
    )
    fun queryDownloadTask(downloadId: Long): Flowable<Array<DownloadTaskEntity>>

    @Delete
    fun deleteDownloadTasks(vararg tasks: DownloadTaskEntity):Single<Int>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDownloadTasks(vararg tasks: DownloadTaskEntity): Single<LongArray>

    @Update
    fun updateDownloadTasks(vararg tasks: DownloadTaskEntity): Single<Int>

}