package com.gitlab.frost.frostdown.thread

import android.content.Context
import android.content.SharedPreferences
import androidx.core.content.edit
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.FlowableOnSubscribe
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.lang.IllegalArgumentException
import java.util.concurrent.Executor
import java.util.concurrent.ThreadPoolExecutor

object ParallelManger {
    const val PARALLEL_NUM = "parallel_num"
    const val PARALLEL_MANGER = "parallel_manger"
    private var parallelNum = 0
    fun Context.getParallelNum(): Int {
        if (parallelNum == 0) {
            parallelNum =
                getSharedPreferences(PARALLEL_MANGER, Context.MODE_PRIVATE).getInt(PARALLEL_NUM, 3)
        }
        return parallelNum
    }

    fun Context.setPARALLELNum(coreNum: Int): Boolean {
        if (coreNum == 0) {
            throw IllegalArgumentException("core quantity must be large 0")
        }
        val edit = getSharedPreferences(PARALLEL_MANGER, Context.MODE_PRIVATE).edit()
        edit.putInt(PARALLEL_NUM + 1, coreNum)
        return edit.commit()
    }
    fun taskPool():Scheduler{
        return Schedulers.io()
    }
}

