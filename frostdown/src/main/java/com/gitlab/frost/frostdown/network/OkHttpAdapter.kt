package com.gitlab.frost.frostdown.network

import okhttp3.OkHttpClient
import okhttp3.Response
import okhttp3.internal.toLongOrDefault
import okio.IOException
import java.io.InputStream
import java.text.SimpleDateFormat
import java.util.*

class OkHttpAdapter : AbstractNetworkAdapter<OkHttpAdapter.OkHttpResponseAdapter>() {
    companion object {
        val client = OkHttpClient.Builder()
            .build()
    }

    inner class OkHttpResponseAdapter(private val response: Response) : AbstractResponseAdapter() {
        override fun acceptRange(): Boolean {
            return response.acceptRange()
        }

        override fun getCode(): Int {
            return response.code
        }

        override fun onResponse(
            handler: (InputStream) -> Unit,
            onCatch: (Exception) -> Unit,
            onFinally: () -> Unit
        ) {
            try {
                val inputStream = response.body?.byteStream()
                if (inputStream == null) {
                    onCatch(NullPointerException("response body is null"))
                } else {
                    handler(inputStream)
                }
            } catch (e: Exception) {
                onCatch(e)
            } finally {
                onFinally()
            }
        }

        override fun getETag(): String {
            return response.getETag()
        }

        override fun getLastModified(): Long {
            return response.getLastModified()
        }

        override fun getContentLength(): Long {
            return response.getContentLength()
        }

        override fun close() {
            response.close()
        }

        private val pattern = "EEE, d MMM yyyy HH:mm:ss z"
        private fun okhttp3.Response.getContentLength() =
            this.headers["Content-Length"]?.toLongOrDefault(-1)!!

        private fun okhttp3.Response.getETag() = this.headers["ETag"] ?: ""
        private fun okhttp3.Response.getLastModified() = this.headers["Last-Modified"]?.run {
            SimpleDateFormat(
                pattern,
                Locale.US
            ).parse(this)
        }?.time ?: 0L

        private fun okhttp3.Response.acceptRange() =
            this.headers["Accept-Ranges"] == "bytes"

        private fun okhttp3.Response.getContentRange() = this.headers["Content-Range"]
    }

    @Throws(IOException::class)
    override fun getResponse(request: Request): OkHttpResponseAdapter {
        val okRequest = okhttp3.Request.Builder()
            .url(request.url)
            .apply {
                request.headers.forEach {
                    addHeader(it.first, it.second)
                }
            }
            .build()
        val response = client.newCall(okRequest)
            .execute()
        return OkHttpResponseAdapter(response)
    }
}