package com.gitlab.frost.frostdown.dispatcher

import java.io.File
import java.io.IOException
import java.lang.IllegalArgumentException

interface DownloadStrategy {
    fun start()
    fun stop()
    fun resume()
    fun cancel()
}
typealias DownloadSizeListener = (Long) -> Unit

abstract class AbstractDownloadStrategy : DownloadStrategy {
    var status: Status = Status.CREATED
        protected set
    protected lateinit var file: File
    @Throws(IOException::class)
    protected fun existsFile(name: String, saveDir: String): Boolean {
        val dir = File(saveDir)
        if (!dir.isDirectory) {
            throw IllegalArgumentException("saveDir is not directory")
        }
        file = File(dir, name)
        if (file.exists()) {
            return true
        }
        file.createNewFile()
        return false
    }

    protected val listenerList = mutableListOf<DownloadSizeListener>()
    fun addDownloadProgressListener(listener: DownloadSizeListener) {
        listenerList.add(listener)
    }

    enum class Status {
        CREATED,
        STARTED,
        STOP,
        CANCELED
    }
}