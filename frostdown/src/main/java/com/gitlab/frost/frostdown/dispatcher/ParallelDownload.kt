package com.gitlab.frost.frostdown.dispatcher

import android.util.Log
import com.gitlab.frost.frostdown.fileinfo.BlockInfo
import com.gitlab.frost.frostdown.fileinfo.FileInfo
import com.gitlab.frost.frostdown.network.AbstractNetworkAdapter
import com.gitlab.frost.frostdown.network.AbstractResponseAdapter
import com.gitlab.frost.frostdown.network.Request
import com.gitlab.frost.frostdown.thread.ParallelManger
import com.gitlab.frost.logger.Logger
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import okhttp3.internal.notifyAll
import okhttp3.internal.wait
import java.io.File
import java.io.IOException
import java.lang.IllegalArgumentException
import java.util.*
import java.util.concurrent.BlockingDeque
import java.util.concurrent.ConcurrentHashMap
import java.util.concurrent.ConcurrentLinkedQueue
import kotlin.Exception


class ParallelDownload(
    private val fileInfo: FileInfo,
    private val netAdapter: AbstractNetworkAdapter<out AbstractResponseAdapter>
) : AbstractDownloadStrategy() {
    private val taskQueue = ConcurrentLinkedQueue<BlockInfo>()
    private val runningTasks = ConcurrentHashMap<BlockInfo, Disposable>()
    private lateinit var guardDisposable: Disposable
    private val downloadSizeThread = Schedulers.single()
    private var downloadSize = 0L

    init {
        init()
    }

    private fun init() {
        val newDownload = !existsFile(fileInfo.fileName(), fileInfo.saveDir())
        fileInfo.getBlocks(file) {
            it.onEach {
                downloadSize += it.getDownloadLength()
            }
            taskQueue.addAll(it)
        }
        addDownloadProgressListener {
            if (it == fileInfo.getTotal()) {
                Logger.debug("file:${fileInfo.fileName()} download completed")
            }
        }
    }

    private val guardTask = Observable.create<Pair<BlockInfo, Flowable<Long>>> { emitter ->
        try {
            while (status!=Status.STOP) {
                val task = taskQueue.poll()
                task?.also {
                    emitter.onNext(it to startTask(it))
                }
            }
        } catch (e: Exception) {
            emitter.onError(e)
        } finally {
            emitter.onComplete()
        }
    }.subscribeOn(Schedulers.newThread())

    private fun startTask(blockInfo: BlockInfo): Flowable<Long> {
        val bytes = ByteArray(1024)
        var len = 0
        var current = blockInfo.needRange().first
        val end = blockInfo.needRange().second
        return Flowable.create<Long>({ emitter ->
            netAdapter.getResponse(
                Request(
                    fileInfo.getUrl(),
                    listOf(
                        "Range" to "$current-$end"
                    )
                )
            ).onResponse(
                handler = { inputStream ->
                    while (inputStream.read(bytes).also {
                            len = it
                        } != -1
                    ) {
                        stopThread()
                        current += len
                        blockInfo.writeFile(bytes, len)
                        emitter.onNext(len.toLong())
                    }
                },
                onCatch = {
                    taskQueue.offer(blockInfo)
                    emitter.onError(it)
                },
                onFinally = {
                    runningTasks[blockInfo]?.dispose()
                    runningTasks.remove(blockInfo)
                    emitter.onComplete()
                }
            )
        }, BackpressureStrategy.LATEST)
            .subscribeOn(ParallelManger.taskPool())
            .observeOn(downloadSizeThread)
    }

    override fun start() {
        if (status == Status.CREATED) {
            guardDisposable = guardTask
                .doOnDispose {
                    runningTasks.forEach { (b: BlockInfo, d: Disposable) ->
                        d.dispose()
                    }
                    runningTasks.clear()
                }
                .subscribe {
                    runningTasks[it.first] = it.second.subscribe { len ->
                        downloadSize += len
                        dispatcherProgress(downloadSize)
                    }
                }
            status = Status.STARTED
        }
    }

    private fun dispatcherProgress(size: Long) {
        Log.d("test",size.toString())
        listenerList.onEach {
            it(size)
        }
    }

    override fun stop() {
        if (status == Status.STARTED) {
            status = Status.STOP
        }
    }

    override fun resume() {
        if (status == Status.STOP) {
            resumeAllThread()
            status = Status.STARTED
        }
    }

    override fun cancel() {
        if (!guardDisposable.isDisposed) {
            guardDisposable.dispose()
        }
    }

    @Synchronized
    private fun stopThread() {
        if (status == Status.STOP) {
            this.wait()
        }
    }

    @Synchronized
    private fun resumeAllThread() {
        if (status == Status.STOP) {
            this.notifyAll()
        }
    }
}

